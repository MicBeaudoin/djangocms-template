import sys
from subprocess import call

def main():

    print '*** Creating virtual environment ***'
    call('virtualenv ../env/{{ project_name }} -p /usr/bin/python3', shell=True)
    call('source ../env/{{ project_name }}/bin/activate', shell=True)

    print '*** Installing requirements ***'
    call('pip install -r requirements/local.txt', shell=True)

    # Migrate database
    call('python manage.py migrate', shell=True)

    # Create super user
    call('python manage.py createsuperuser', shell=True)

if __name__ == '__main__':
    main()
    sys.exit(0)
